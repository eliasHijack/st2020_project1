let sorting = (array) => {
	//console.log(array)
	let temp=0;
	for(let i=0; i<array.length;i++){
		for(var j=0;j<array.length-i;++j){
			if(array[j]>array[j+1]){
				temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}			
	}
	//console.log(array)
	return array;
}

let compare = (a, b) => {
	//for(key in Object. keys(a)){
	//	console.log(key)	
	//}
	const idList = Object.keys(a);
	//console.log(idList);
	var i=0;
	for(;i<idList.length;i++){
		if(idList[i]=='PM2.5'){
			break;
		}
	}
	if(parseFloat(Object. values(a)[i])>parseFloat(Object. values(b)[i])){
		//console.log("1")
		return 1;
	}
	if(parseFloat(Object. values(a)[i])<parseFloat(Object. values(b)[i])){
		//console.log("-1")
		return -1;
	}
	return 0;
}

let average = (nums) => {
	let toAvg = 0;
	for(let k=0;k<nums.length;k++){
		toAvg=toAvg+nums[k];
	}
	toAvg=toAvg/nums.length;
	return Math.round(toAvg* 100) / 100;
} 


module.exports = {
    sorting,
    compare,
    average
}
